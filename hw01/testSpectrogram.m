
clc; clear; close all;
load handel.mat;
s.freq = 1000;
s.fs = Fs;
s.signal = y;
s.window = 512;
spect(s)

%%
s.window = 1024;
spect(s)

%%
s.window = 128;
spect(s)