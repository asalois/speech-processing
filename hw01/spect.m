% calculate Pi Dartboard Method
function spect(s)

%     sound(s.signal, s.fs);
%% calc time scale
t = 1:length(s.signal);
t = t/s.fs;

%% calc spectrum
L = length(s.signal);
n = nextpow2(L);
L = 2^n;
sig = [s.signal;  zeros(L-length(s.signal),1)]; % make fft use pow of 2
x = fft(sig);
P2 = abs(x/L);
xR = P2(1:L/2+1);
xR(2:end-1) = 2*xR(2:end-1);
zerodB = (xR - min(xR));
zerodB = zerodB / max(zerodB);
lgScale = db(zerodB, 'power');
size(lgScale)
f = s.fs*(0:(L/2))/L;

%% calc spectrogram
sig = [s.signal;  zeros(s.window-mod(length(s.signal),s.window),1)]; % add zeros if needed
folded = reshape (sig,s.window,length(sig)/s.window); % fold signal by window size
x2d = fft(folded); % take fft of each column (window)
P2 = abs(x2d/s.window); % get real part
P2 = P2 - min(P2,[],"all"); % only postive
P2 = P2 / max(P2,[],"all"); % scale
lgScaled2d = zeros(s.window/2+1,size(folded,2)); % to hold spec
f2d = s.fs*(0:(s.window/2))/s.window; % the freqs for spec
for i = 1:size(folded,2)
    xR = P2(1:s.window/2+1,i); % trunc only need postive freqs
    xR(2:end-1) = 2*xR(2:end-1);
    lgScaled2d(:,i) = db(xR, 'power'); % get in dB
end

lgScaled2d(lgScaled2d < -30) = -30;

%% plot stuff
figure()
subplot(2,2,1)
plot(f,lgScale)
title('Single-Sided Amplitude Spectrum of x(t)')
xlabel('f (Hz)')
ylabel('dB(|P1(f)|)')
camroll(90)

subplot(2,2,2)
x = [0 t(end)];
y = [0 s.fs/2];
imagesc(x,y,lgScaled2d)
axis xy
colorbar

subplot(2,2,4)
plot(t,s.signal)
title('Singal x(t)')
xlabel('Time')
ylabel('Power')

end