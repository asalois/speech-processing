function [mu,sigma,w,nn] = em(numGaus,data,iters)
% Expectation Maxiamation using Gaussians Assmuming 2D
[~,c] = kmeans(data,numGaus);
mu = c;
% mu = data(randperm(length(data),numGaus),:);
muNext = zeros(numGaus,2);
sigma = ones(2,2,numGaus);
w = ones(length(data),numGaus)/numGaus;
nn = norm(muNext - mu);
%%
if iters > 0
    for n = 1:iters
        if iters == 0
            % M step
            [~,sigma,p] = maxi(w,mu,sigma,data,numGaus);
            muNext = mu;
            mu = zeros(size(muNext));
        else
            % M step
            [muNext,sigma,p] = maxi(w,mu,sigma,data,numGaus);
        end
        % E step
        w = expect(w,mu,sigma,data,numGaus,p);
        nn = norm(muNext - mu);
        mu = muNext;
    end
else
    iters = 0;
    while nn > 10
        if iters == 0
            % M step
            [~,sigma,p] = maxi(w,mu,sigma,data,numGaus);
            muNext = mu;
            mu = zeros(size(muNext));
        else
            % M step
            [muNext,sigma,p] = maxi(w,mu,sigma,data,numGaus);
        end
        % E step
        w = expect(w,muNext,sigma,data,numGaus,p);
        nn = norm(muNext - mu);
        mu = muNext;
        iters = iters + 1;
    end

end

end