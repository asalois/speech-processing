clc; clear; close all;
load("HW4_data.mat");
%%
tic
for i = 3:12
%     i  = 7
    rng(10)
    [m,s,w,n] = em(i,data,0);
    plot_data(m,s,data,i)
end
toc
%     theta_grid = linspace(0,2*pi);
%     el = 2*sqrt(5.991*s);
% 
%     for k = 1:i
%         ellipse_x_r  = el(k,1)*cos(theta_grid);
%         ellipse_y_r  = el(k,2)*sin(theta_grid);
%         r_ellipse = [ellipse_x_r; ellipse_y_r]';
%         plot(r_ellipse(:,1) + m(k,1),r_ellipse(:,2) + m(k,2),'r-')
%     end
%     hold off
% end
