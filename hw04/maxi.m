function [mu,sigma,p] = maxi(w,mu,sigma,data,numGaus)
% Maxiamation using Gaussians Assmuming 2D
L = length(data);
p = zeros(numGaus,1);
parfor k = 1:numGaus
    bigSigma = zeros(2,2,length(data));
    for i = 1:L
        df = data(i,:) - mu(k,:);
        bigSigma(:,:,i) = w(i,k).*df'*df;
    end
    sigma(:,:,k) = sum(bigSigma,3) ./ sum(w(:,k));
    mu(k,:) = sum(w(:,k).*data) / sum(w(:,k));
    p(k) = mean(w(:,k));
end
end