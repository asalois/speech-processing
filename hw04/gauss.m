function y = gauss(x,mu,sigma)
% calc value of multivariate gaussian
df = x - mu;
power = exp(-1/2*df*inv(sigma)*df');
factor = 1/(2*pi)* 1/sqrt(det(sigma));
y = factor * power;
end