function plot_data(m,s,d,i)
% Plot the data and the means
f = figure("Position", [10,50,1200,1000]);
ftitle = sprintf("%d Guasians K-Means Init", i);
fname = sprintf("%02d_guasians_kmeans.png", i);
hold on
scatter(d(:,1), d(:,2),'black','d','filled')
scatter(m(:,1),m(:,2),'blue','filled')
xlim([-500, 900]);
ylim([-600, 700]);
title(ftitle)

for k = 1:i
    [eigenvec, eigenval ] = eig(s(:,:,k));

    % Get the index of the largest eigenvector
    [largest_eigenvec_ind_c, r] = find(eigenval == max(max(eigenval)));
    largest_eigenvec = eigenvec(:, largest_eigenvec_ind_c);

    % Get the largest eigenvalue
    largest_eigenval = max(max(eigenval));

    % Get the smallest eigenvector and eigenvalue
    if(largest_eigenvec_ind_c == 1)
        smallest_eigenval = max(eigenval(:,2));
        smallest_eigenvec = eigenvec(:,2);
    else
        smallest_eigenval = max(eigenval(:,1));
        smallest_eigenvec = eigenvec(1,:);
    end

    % Calculate the angle between the x-axis and the largest eigenvector
    angle = atan2(largest_eigenvec(2), largest_eigenvec(1));

    % This angle is between -pi and pi.
    % Let's shift it such that the angle is between 0 and 2pi
    if(angle < 0)
        angle = angle + 2*pi;
    end

    % Get the 95% confidence interval error ellipse
    chisquare_val = 2.4477;
    theta_grid = linspace(0,2*pi,1000);
    phi = angle;
    X0=m(k,1);
    Y0=m(k,2);
    a=chisquare_val*sqrt(largest_eigenval);
    b=chisquare_val*sqrt(smallest_eigenval);

    % the ellipse in x and y coordinates
    ellipse_x_r  = a*cos( theta_grid );
    ellipse_y_r  = b*sin( theta_grid );

    %Define a rotation matrix
    R = [ cos(phi) sin(phi); -sin(phi) cos(phi) ];

    %let's rotate the ellipse to some angle phi
    r_ellipse = [ellipse_x_r;ellipse_y_r]' * R;

    % Draw the error ellipse
    plot(r_ellipse(:,1) + X0,r_ellipse(:,2) + Y0,'r-')
end
hold off
exportgraphics(f,fname,'Resolution',300)
end