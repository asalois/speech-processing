function w = expect(w,mu,sigma,data,numGaus,pm)
% Epectation using Gaussians Assmuming 2D
wNext = zeros(size(w));
L = length(data);
parfor k = 1:numGaus
    m = mu(k,:);
    s = sigma(:,:,k);
    for i = 1:L
        d = data(i,:);
        p = gauss(d,m,s);
        if p == 0
            wNext(i,k) = 0;
        else
            wNext(i,k) = p*pm(k);
        end
    end
end
wNext = wNext ./ sum(wNext,2);
w = wNext/sum(w,"all");
end