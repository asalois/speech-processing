
clc; clear; close all;
%% Open speech waveform file in TIMIT database
% filename = 'C:/Users/v16b915/Documents/TIMIT/TIMIT/TRAIN/DR8/FCLT0/SI808';
filename = 'H:/Documents/TIMIT/TIMIT/TRAIN/DR8/FCLT0/SI808';
fid = fopen([filename,'.wav']);
status = fseek(fid,1024, -1); % skip header
[waveform,count]=fread(fid,inf,'int16'); % read speech data
fclose(fid);
signal.fs = 16000;
signal.signal = waveform;
% soundsc(waveform,signal.fs)

[signal.sent] = readvars([filename,'.TXT'],"FileType","text");
[signal.startW,signal.endW,signal.words] = readvars([filename,'.WRD'],"FileType","text", 'Delimiter',' ');
[signal.startP,signal.endP,signal.phons] = readvars([filename,'.PHN'],"FileType","text", 'Delimiter',' ');

%% 
phidx = 2
phoneme=signal.phons(phidx)
x=signal.signal(signal.startP(phidx):signal.endP(phidx))

%%
