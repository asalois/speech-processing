function [cc,ccidx] = getSentData(num,onCluster)
%GETSENTDATA Summary of this function goes here
%   Detailed explanation goes here
vowels = [{'iy'}; {'ih'}; {'eh'}; {'ey'}; {'ae'}; {'aa'}; {'aw'}; {'ay'}; ...
    {'ah'}; {'ao'}; {'oy'}; {'ow'}; {'uh'}; {'uw'}; {'ux'}; {'er'}];
cut = 8;
sig = cell(length(num),1);
cc = cell(length(num),1);
ccidx = cell(length(num),1);
for k = num
    timitTable = readtable('all-timit.txt','Delimiter','/');
    filename = getPath(table2cell(timitTable(k,:)),onCluster);
    fid = fopen(filename+'.WAV');
    status = fseek(fid,1024, -1); % skip header
    [waveform,~]=fread(fid,inf,'int16'); % read speech data
    fclose(fid);
    signal.fs = 16000;
    signal.signal = waveform;
    [signal.sent] = readvars(filename +'.TXT',"FileType","text");
    [signal.startW,signal.endW,signal.words] = readvars(filename+'.WRD',"FileType","text", 'Delimiter',' ');
    [signal.startP,signal.endP,signal.phons] = readvars(filename+'.PHN',"FileType","text", 'Delimiter',' ');
    n = size(signal.startP,1);
    j = 1;
    for i = 1:n
        idx = cellfun(@isequal, vowels, repmat(signal.phons(i), size(vowels)));
        if any(idx)
            c = cep(signal.signal(signal.startP(i)+1:signal.endP(i)),256,128);
            c = mean(c,2);
            signal.cc(:,j) = [real(c(1:cut)); imag(c(1:cut))];
            signal.cc(isinf(signal.cc)|isnan(signal.cc)) = 0;
            signal.ccidx(:,j) = idx;
            j = j + 1;
        end
    end
    idx = k+1-num(1);
    sig(idx) = {signal};
    cc(idx) = {signal.cc};
    ccidx(idx) = {signal.ccidx};
%     save(filename)
%     sig{k - num(1) +1} = signal;
end
cc = cell2mat(cc');
ccidx = cell2mat(ccidx');
end



