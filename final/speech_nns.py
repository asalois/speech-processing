# baseline cnn model for mnist
import os
import sys
from speech_models import define_model
from numpy import mean
from numpy import std
import numpy as np
import scipy.io as spio
import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn.model_selection import KFold
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.callbacks import EarlyStopping

#mirrored_strategy = tf.distribute.MirroredStrategy()
idx = int(sys.argv[1])
models = np.arange(5)
mats = np.arange(5)
comb = np.array(np.meshgrid(models, mats)).T.reshape(-1,2)
modelNumber = comb[idx,0]
matNumber = comb[idx,1]
print('Model Number: %d' % modelNumber)

# load train and test dataset
def load_dataset():
	matNames = ['data8.mat', 'data10.mat', 'data16.mat', 'data20.mat', 'data32.mat']
	matname = matNames[matNumber]
	mat = spio.loadmat(matname, squeeze_me=False)
	x_train = mat['xtrain']
	y_train = mat['ytrain']
	x_test = mat['xtest']
	y_test = mat['ytest']

	# Convert the data to floats between 0 and 1 and transpose matrix
	x_train = x_train.astype('float32')
	y_train = y_train.astype('float32')
	x_test = x_test.astype('float32')
	y_test = y_test.astype('float32')
	x_train = np.transpose(x_train)
	y_train = np.transpose(y_train)
	x_test = np.transpose(x_test)
	y_test = np.transpose(y_test)

	print(x_train.shape, 'train samples')
	print(y_train.shape, 'train labels')
	print(x_test.shape, 'test samples')
	print(y_test.shape, 'test labels')
	trainX = x_train
	trainY = y_train
	testX = x_test
	testY = y_test
	return trainX, trainY, testX, testY
 

# evaluate a model using k-fold cross-validation
def evaluate_model(dataX, dataY, testX, testY, vb, n_folds, eps):
	bsz = 16
	scores, histories = list(), list()
	# set up early stopping
	es = EarlyStopping(monitor='val_accuracy', mode='min', verbose=1, patience=3)
	# show model
	inputDim = testX.shape[1]
	classes = testY.shape[1]
	model = define_model(modelNumber,inputDim,classes)
	model.summary()
   	# prepare cross validation
	kfold = KFold(n_folds, shuffle=True, random_state=1)
	# enumerate splits
	for train_ix, test_ix in kfold.split(dataX):
		# define model
		model = define_model(modelNumber,inputDim,classes)
		# select rows for train and test
		trainX, trainY, valX, valY = dataX[train_ix], dataY[train_ix], dataX[test_ix], dataY[test_ix]
		# fit model
		history = model.fit(trainX, trainY, epochs=eps, batch_size=bsz, \
                        validation_data=(valX, valY), verbose=vb, callbacks=[es])
		# evaluate model
		score = model.evaluate(trainX, trainY, verbose=vb)
		print('Final Training loss: %1.5f' % score[0])
		print('Final Training acc:  %1.5f' % score[1])
		score = model.evaluate(testX, testY, verbose=vb)
		print('Test loss: %1.5f' % score[0])
		print('Test acc:  %1.5f' % score[1])
		# stores scores
		scores.append(score[1])
		histories.append(history)
	return scores, histories
 
# plot diagnostic learning curves
def summarize_diagnostics(histories):
	for i in range(len(histories)):
		# plot loss
		plt.subplot(2, 1, 1)
		plt.title('Cross Entropy Loss')
		plt.plot(histories[i].history['loss'], color='blue', label='train')
		plt.plot(histories[i].history['val_loss'], color='orange', label='test')
		# plot accuracy
		plt.subplot(2, 1, 2)
		plt.title('Classification Accuracy')
		plt.plot(histories[i].history['accuracy'], color='blue', label='train')
		plt.plot(histories[i].history['val_accuracy'], color='orange', label='test')
	plt.savefig('loss.png')
 
# summarize model performance
def summarize_performance(scores):
	# print summary
	print('Accuracy for model %d and mat %d : mean=%1.4f std=%1.4f, n=%d' % (modelNumber, matNumber, mean(scores), std(scores), len(scores)))
	print('Results %d,%d,%1.7f,%1.7f,%d' % (modelNumber, matNumber, mean(scores), std(scores), len(scores)))
	# box and whisker plots of results
	#plt.boxplot(scores)
	#plt.savefig('boxplot.png')
 
# run the test harness for evaluating a model
def run_test_harness():
	# load dataset
	trainX, trainY, testX, testY = load_dataset()
	# evaluate model
	scores, histories = evaluate_model(trainX, trainY, testX, testY, 2, 10, 20)
	# learning curves
	#summarize_diagnostics(histories)
	# summarize estimated performance
	summarize_performance(scores)
 
# entry point, run the test harness
run_test_harness()
