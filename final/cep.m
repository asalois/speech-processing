function cceof = cep(signal,fftL,overlap)
%CEP Summary of this function goes here
%   Detailed explanation goes here
L = length(signal);
n = ceil(L/overlap);
if ~ (floor(L/overlap) == (L/overlap))
signal = [signal; zeros(n*overlap - L,1)];
end
s = zeros(fftL,n-1);
for i = 1:n-1
    k = (i-1)*overlap;
    s(:,i) = signal(k+1:k+fftL);
end
f = fft(s);
cf  = fft(log(f));
% plot([abs(cf) abs(f)])
cceof = cf;
end

