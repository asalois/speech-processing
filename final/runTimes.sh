#!/bin/bash
grep real speechCPU*.err | cut -d ' ' -f 2 > cpuTimes.txt
grep real speechGPU*.err | cut -d ' ' -f 2 > gpuTimes.txt
grep Results speech*.out | cut -d ' ' -f 2 > results.csv
