import scipy.io as spio
import numpy as np

# Load the data
matname = 'data.mat'
print(matname)
mat = spio.loadmat(matname, squeeze_me=False)
x_train = mat['xtrain']
y_train = mat['ytrain']
x_test = mat['xtest']
y_test = mat['ytest']

# Convert the data to floats between 0 and 1 and transpose matrix
x_train = x_train.astype('float32')
y_train = y_train.astype('float32')
x_test = x_test.astype('float32')
y_test = y_test.astype('float32')
x_train = np.transpose(x_train)
y_train = np.transpose(y_train)
x_test = np.transpose(x_test)
y_test = np.transpose(y_test)


print(x_train.shape, 'train samples')
print(y_train.shape, 'train labels')
print(x_test.shape, 'test samples')
print(y_test.shape, 'test labels')
