function makeNNData(num,onCluster)
%GETSENTDATA Summary of this function goes here
%   Detailed explanation goes here
if num == 1
    timitTable = readtable('all-timit.txt','Delimiter','/');
    filename = getPath(table2cell(timitTable(num,:)),onCluster)
    load(filename)
    x = 1
else
%     sig = cell(length(num),1);
    for k = num
        timitTable = readtable('all-timit.txt','Delimiter','/');
        filename = getPath(table2cell(timitTable(k,:)),onCluster);
        fid = fopen(filename+'.WAV');
        status = fseek(fid,1024, -1); % skip header
        [waveform,~]=fread(fid,inf,'int16'); % read speech data
        fclose(fid);
        signal.fs = 16000;
        signal.signal = waveform;
        [signal.sent] = readvars(filename +'.TXT',"FileType","text");
        [signal.startW,signal.endW,signal.words] = readvars(filename+'.WRD',"FileType","text", 'Delimiter',' ');
        [signal.startP,signal.endP,signal.phons] = readvars(filename+'.PHN',"FileType","text", 'Delimiter',' ');
        n = size(signal.startP,1);
        cut = 20;
        signal.cc = zeros(cut,n);
        for i = 1:n
            c = cep(signal.signal(signal.startP(i)+1:signal.endP(i)),256,128);
            c = mean(c,2);
            signal.cc(:,i) = c(1:cut);
        end
        sig = signal;
        save(filename)
%         sig{k - num(1) +1} = signal;
    end
end
