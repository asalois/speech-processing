function phons = getPhonemes(num,onCluster)
%GETPHONEME Summary of this function goes here
%   Detailed explanation goes here
timitTable = readtable('all-timit.txt','Delimiter','/');
if length(num) == 1
    filename = getPath(table2cell(timitTable(num,:)),onCluster);
    [~,~,phon] = readvars(filename + ".PHN","FileType","text", 'Delimiter',' ');
    phons = phon;
elseif length(num) > 1
    phons = cell(length(num),1);
    for i = num
        filename = getPath(table2cell(timitTable(i,:)),onCluster);
        [~,~,phon] = readvars(filename + ".PHN","FileType","text", 'Delimiter',' ');
        phons{i - num(1) +1,:} = phon';
    end
end
end


