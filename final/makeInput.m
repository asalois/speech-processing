function signals = makeInput(num,onCluster)
%MAKEINPUT Summary of this function goes here
%   Detailed explanation goes here
timitTable = readtable('all-timit.txt','Delimiter','/');
if length(num) == 1
    filename = getPath(table2cell(timitTable(num,:)),onCluster);
    fid = fopen(filename + ".WAV");
    status = fseek(fid,1024, -1); % skip header
    [waveform,count] = fread(fid,inf,'int16'); % read speech data
    fclose(fid);
    signal.fs = 16000;
    signal.signal = waveform;
    [signal.sent] = readvars(filename + ".TXT","FileType","text");
    [signal.startW,signal.endW,signal.words] = readvars(filename + ".WRD","FileType","text", 'Delimiter',' ');
    [signal.startP,signal.endP,signal.phons] = readvars(filename + ".PHN","FileType","text", 'Delimiter',' ');
    signals = signal;
elseif length(num) > 1
    signals = cell(length(num),1);
    for i = num
        filename = getPath(table2cell(timitTable(i,:)),onCluster);
        fid = fopen(filename + ".WAV");
        status = fseek(fid,1024, -1); % skip header
        [waveform,count] = fread(fid,inf,'int16'); % read speech data
        fclose(fid);
        signal.fs = 16000;
        signal.signal = waveform;
        [signal.sent] = readvars(filename + ".TXT","FileType","text");
        [signal.startW,signal.endW,signal.words] = readvars(filename + ".WRD","FileType","text", 'Delimiter',' ');
        [signal.startP,signal.endP,signal.phons] = readvars(filename + ".PHN","FileType","text", 'Delimiter',' ');
        signals{i - num(1) +1} = signal;
    end
end
end

