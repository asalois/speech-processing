from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.optimizers import SGD

def define_model(modelNum,inputDim,classes):
    if modelNum == 0:
        model = Sequential()
        model.add(Dense(128, activation='relu', kernel_initializer='he_uniform', input_dim=inputDim))
        model.add(Dense(classes, activation='softmax'))
        # compile model
        opt = SGD(learning_rate=0.01, momentum=0.9)
        model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    if modelNum == 1:
        model = Sequential()
        model.add(Dense(1024, activation='relu', kernel_initializer='he_uniform', input_dim=inputDim))
        model.add(Dense(classes, activation='softmax'))
        # compile model
        opt = SGD(learning_rate=0.01, momentum=0.9)
        model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    if modelNum == 2:
        model = Sequential()
        model.add(Dense(1024, activation='relu', kernel_initializer='he_uniform', input_dim=inputDim))
        model.add(Dense(256, activation='relu', kernel_initializer='he_uniform'))
        model.add(Dense(classes, activation='softmax'))
        # compile model
        opt = SGD(learning_rate=0.01, momentum=0.9)
        model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    if modelNum == 3:
        model = Sequential()
        model.add(Dense(1024, activation='relu', kernel_initializer='he_uniform', input_dim=inputDim))
        model.add(Dense(256, activation='relu', kernel_initializer='he_uniform'))
        model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
        model.add(Dense(classes, activation='softmax'))
        # compile model
        opt = SGD(learning_rate=0.01, momentum=0.9)
        model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    if modelNum == 4:
        model = Sequential()
        model.add(Dense(4096, activation='relu', kernel_initializer='he_uniform', input_dim=inputDim))
        model.add(Dense(classes, activation='softmax'))
        # compile model
        opt = SGD(learning_rate=0.01, momentum=0.9)
        model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    return model
