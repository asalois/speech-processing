#!/bin/bash
singularity pull tensorflow.sif  docker://tensorflow/tensorflow:latest
singularity pull tfgpu.sif docker://nvcr.io/nvidia/tensorflow:22.07-tf2-py3
