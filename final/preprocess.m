clear;clc;
% phons = getPhonemes(1:63000,false)

%%
c=parcluster;
c.AdditionalProperties.WallTime = '0-0:20:00';
c.AdditionalProperties.MemUsage='2gb';
c.AdditionalProperties.QueueName='legacy';
c.AdditionalProperties.AccountName='group-rci';                 
c.AdditionalProperties.GpusPerNode = 0;

%%
job =createJob(c);
numPerJob = 100;
for i = 1:63
    createTask(job, @getSentData, 2, {(i-1)*numPerJob+1:i*numPerJob,true});   
end
submit(job) 

%%
wait(job)
job_output = fetchOutputs(job);
cc = cell2mat(job_output(1,1));
ccidx = cell2mat(job_output(1,2));
for i = 2:63
    cc = [cc cell2mat(job_output(i,1))];
    ccidx = [ccidx cell2mat(job_output(i,2))];
end
%%
xtest = cc(:,1:1680);
ytest = ccidx(:,1:1680);
xtrain = cc(:,1681:end);
ytrain = ccidx(:,1681:end);
fileName = 'data8';
save(fileName, "ytrain","xtrain","xtest","ytest")

%% 
cpu = readmatrix('cpuTimes.txt');
gpu = readmatrix('gpuTimes.txt');
gpu(gpu>900) = 350;

f=figure()
subplot(2,1,1)
histogram(cpu)
subplot(2,1,2)
boxplot(cpu,'Orientation','horizontal')
sgtitle('CPU Time')
exportgraphics(f,'cpuTimes.png','Resolution',300)

f=figure()
subplot(2,1,1)
histogram(gpu)
subplot(2,1,2)
boxplot(gpu,'Orientation','horizontal')
sgtitle('GPU Time')
exportgraphics(f,'gpuTimes.png','Resolution',300)

%%
res = readmatrix('results.csv');
acc = res(:,3)
f=figure()
subplot(2,1,1)
histogram(acc)
subplot(2,1,2)
boxplot(acc,'Orientation','horizontal')
sgtitle('Accuarcy')
exportgraphics(f,'acc.png','Resolution',300)